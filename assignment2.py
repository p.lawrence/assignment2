from flask import Flask, render_template, request, redirect, url_for, jsonify, json, Response

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]


@app.route('/book/JSON')
def bookJSON():
	r = json.dumps(books)
	return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
	# <your code>
	return render_template('showPage.html', books=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
	if request.method == 'POST':
		new_book = request.form['name']
		new_id = 0
		for book in books:
			if int(book['id']) > new_id:
				new_id = int(book['id'])
		new_id += 1
		id_str = str(new_id)
		new_book = {'title': new_book, 'id': id_str}
		books.append(new_book)
		return render_template('newBook.html', books=books)
	else:
		return render_template('newBook.html', books=books)

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
	if request.method == 'POST':
		new_name = request.form['name']
		for book in books:
			if book['id'] == str(book_id):
				book['title'] = str(new_name)
		return render_template('editBook.html', books=books, book_id=book_id)
	else:
		return render_template('editBook.html', books=books, book_id=book_id)

	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
	title = None
	for book in books:
		if int(book_id) == int(book['id']):
			title = book['title']

	if request.method == 'POST':
		deleted = None
		for book in books:
			if book['id'] == str(book_id):
				deleted = book
		if deleted:
			books.remove(deleted)
		return render_template('showPage.html', books=books, book_id=book_id)
	else:
		return render_template('deleteBook.html', books=books, book_id=book_id, book_title=title)


if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

